/**
 * 
 */
package edu.westga.cs1302.projectOne.datatier;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**This will be able to load data from an extrnal file
 * @author David Anderson
 *
 * @version Jan 27, 2015
 */
public class TextDataLoader {

	private final File aTextFile;

	/**
	 * Creates a new TextDataLoader for input of the specified text file.
	 * 
	 * @precondition aTextFile != null
	 * @postcondition the object is ready to read or save to the file
	 * 
	 * @param aTextFile
	 *            the I/O file
	 */
	public TextDataLoader(File aTextFile) {
		if (aTextFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.aTextFile = aTextFile;
	}

	/**
	 * Reads all lines from the text file.
	 * 
	 * @precondition none
	 * @return a list of Strings containing the contents of the file
	 * 
	 * @throws IOException
	 *             if the file can't be read
	 */
	public List<String> loadText() throws IOException {
		return Files.readAllLines(this.aTextFile.toPath());
	}

}
