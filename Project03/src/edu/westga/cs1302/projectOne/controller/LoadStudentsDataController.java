/**
 * 
 */
package edu.westga.cs1302.projectOne.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import edu.westga.cs1302.projectOne.datatier.TextFileLoader;
import edu.westga.cs1302.projectOne.datatier.TextLoader;
import edu.westga.cs1302.projectOne.model.DataMap;
import edu.westga.cs1302.projectOne.model.Student;

/**This will control the loading of data from a file to the data map
 * @author David Anderson
 *
 * @version Mar 25, 2015
 */
public class LoadStudentsDataController {

    private DataMap<String, Student> theMap;
    private File inputFile;

    /**
     * Creates a new LoadStudentsController to manage adding movies to the specified
     * map.
     * 
     * @precondition inputFile != null && aMap != null
     * @postcondition the controller is ready to load movies
     * 
     * @param inputFile
     *            the file to read
     * @param aMap
     *            the data map to which movies will be added
     */
    public LoadStudentsDataController(File inputFile,
                                   DataMap<String, Student> aMap) {
        if (inputFile == null) {
            throw new IllegalArgumentException("File to load is null");
        }
        if (aMap == null) {
            throw new IllegalArgumentException("The map was null");
        }

        this.inputFile = inputFile;
        this.theMap = aMap;
    }

    /**
     * Loads movie data from the input file into the map.
     * 
     * @precondition none
     * @postcondition the map contains the movie data
     * 
     * @throws IOException
     *             if the file cannot be read
     */
    public void loadStudentData() throws IOException {
        TextLoader loader = new TextFileLoader(this.inputFile);
        List<String> linesFromFile = loader.loadText();

        for (String oneLineFromFile : linesFromFile) {
            this.addStudent(oneLineFromFile);
        }
    }

    private void addStudent(String oneLineFromFile) throws IOException {
        String[] movieDataArray = oneLineFromFile.split("\\|");
        Student data = new Student(movieDataArray[0],
                                       movieDataArray[1]);
        this.theMap.add(data.getName(), data);
    }
}
