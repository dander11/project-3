/**
 * 
 */
package edu.westga.cs1302.projectOne.controller;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.ListIterator;

/**This will get the the next picture in learning
 * @author David Anderson
 *
 * @version Mar 25, 2015
 */
public class GetNextStudentLearningController {

    private List<String> studentsNames;
    private String currentStudentName;
    private ListIterator<String> iterator;
    
    /**This will create the controller
     * 
     * @param studentList   The list of students as it displayed
     *
     * @precondition        studentList != null
     *
     * @postcondition       The controller will be created
     *
     */
    public GetNextStudentLearningController(List<String> studentList) {
        if (studentList == null) {
            throw new IllegalArgumentException("The map was null");
        }
        
        this.studentsNames = studentList;
        
        this.iterator = this.studentsNames.listIterator();
        this.currentStudentName = this.iterator.next();
        this.currentStudentName = this.iterator.previous();
    }
    
    /**This will return the name of the next student
     * 
     * @precondition    none
     *
     * @return          The name of the next student in the list
     */
    public String getNextStudent() {
        
        
        try {
            this.iterator.set(this.currentStudentName);
            if (this.iterator.hasNext()) {
                this.currentStudentName = this.iterator.next();
            } else {
                while (this.iterator.hasPrevious()) {
                    this.iterator.previous();
                }
                this.currentStudentName = this.iterator.next();
            }
        } catch (ConcurrentModificationException cME) {
            this.iterator = this.studentsNames.listIterator();
            this.iterator.next();
        }
        return this.currentStudentName;
        
    }

}
