/**
 * 
 */
package edu.westga.cs1302.projectOne.controller;

import edu.westga.cs1302.projectOne.model.DataMap;
import edu.westga.cs1302.projectOne.model.Student;

/**This will control when a student is deleted
 * @author David Anderson
 *
 * @version Mar 25, 2015
 */
public class DeleteAStudentController {
    
    private DataMap<String, Student> theMap;
    
    /**This will create the controller
     * 
     * @param aMap  The map the controller will deal with
     *
     * @precondition
     *
     * @postcondition
     */
    public DeleteAStudentController(DataMap<String, Student> aMap) {
        if (aMap == null) {
            throw new IllegalArgumentException("The map is null");
        }
        
        this.theMap = aMap;
        
    }

    /**This will remove the student
     * 
     * @param studentName   The name of the student to be removed
     *
     * @precondition    none
     *
     * @postcondition   the student will be removed
     */
    public void removeStudent(String studentName) {
        this.theMap.remove(studentName);
        
    }
}
