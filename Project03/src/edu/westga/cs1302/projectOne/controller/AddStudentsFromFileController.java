/**
 * 
 */
package edu.westga.cs1302.projectOne.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import edu.westga.cs1302.projectOne.datatier.TextDataLoader;

/**This will control the adding of students names to the lists
 * @author David Anderson
 * 
 * @version 1/27/2015
 */
public class AddStudentsFromFileController {

	private TextDataLoader dataLoader;
	private List<String> textFromFile;

	/**
	 * Creates a new AddTextFromFileController.
	 * 
	 * @precondition 	inputFile != null && existingText != null
	 * @postcondition	the object is ready to get the data from the file
	 *   
	 * @param inputFile		the text file to read
	 * @throws IOException  if the file can't be read
	 */
	public AddStudentsFromFileController(File inputFile) throws IOException {
		if (inputFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.dataLoader = new TextDataLoader(inputFile);
		this.textFromFile = this.readTextFromFile();
	}

	/**
	 * Returns the contents of the text file.
	 * 
	 * @precondition 	none
	 * @return			the lines from the text file
	 * 
	 */
	public List<String> getTextLines()  {
		return this.textFromFile;
	}

	private List<String> readTextFromFile() throws IOException {
		return this.dataLoader.loadText();
	} 
}
