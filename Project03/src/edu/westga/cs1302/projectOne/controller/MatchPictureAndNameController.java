/**
 * 
 */
package edu.westga.cs1302.projectOne.controller;

import edu.westga.cs1302.projectOne.model.DataMap;
import edu.westga.cs1302.projectOne.model.Student;

/**This will control to check if the displayed picture and selected student match
 * @author David Anderson
 *
 * @version Mar 25, 2015
 */
public class MatchPictureAndNameController {

    private DataMap<String, Student> theMap;
    
    /**This will create the controller
     * 
     * @param aMap  the map that holds the student info
     *
     * @precondition    aMap != null
     *
     * @postcondition   The controller will be created
     *
     */
    public MatchPictureAndNameController(DataMap<String, Student> aMap) {
        if (aMap == null) {
            throw new IllegalArgumentException("The map is null");
        }
        
        
        this.theMap = aMap;
    }
    
    /**This compares the currently displayed picture and the selected student name to see if they belong to the same student
     * 
     * @param currentStudentPicUrl  the URL of the image that is currently displayed
     * @param selectedStudentName   the name that the user selects
     *
     * @precondition    currentStudentPicUrl && selectedStudentName != null
     *
     * @return          0 if everything matches 
     */
    public int comparePictureAndName(String currentStudentPicUrl, String selectedStudentName) {
        if (currentStudentPicUrl == null) {
            throw new IllegalArgumentException("Invalid pic Url"); 
        }
        if (selectedStudentName == null) {
            throw new IllegalArgumentException("Invalid name");
        }
        return (currentStudentPicUrl.compareTo(this.theMap.get(selectedStudentName).getStudentPictureUrl().toString()));
    }

}
