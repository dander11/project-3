/**
 * 
 */
package edu.westga.cs1302.projectOne.controller;

import java.util.List;
import java.util.Random;

/**This will control what picture is next when in practice mode
 * @author David Anderson
 *
 * @version Mar 25, 2015
 */
public class GetNextPracticeController {

    private List<String> studentsNames;
    private String currentStudentName;
    private Random randomizer;
    
    /**This will create the controller
     * 
     * @param studentList   The list of students as it displayed
     *
     * @precondition        studentList != null
     *
     * @postcondition       The controller will be created
     *
     */
    public GetNextPracticeController(List<String> studentList) {
        if (studentList == null) {
            throw new IllegalArgumentException("The map was null");
        }
        this.randomizer = new Random();
        this.studentsNames = studentList;
        this.currentStudentName = "";
//      this.
//        this.iterator = this.studentsNames.listIterator();
//        this.currentStudentName = this.iterator.next();
//        this.currentStudentName = this.iterator.previous();
    }
    
    /**
     * Creates a new CharacterSelector instance whose
     * random number generator is seeded with the
     * specified value.
     * 
     * @param studentList   The list of students as it displayed
     * @param seed          the initial value of the internal state
     *                              of the pseudorandom number generator
     */
    public GetNextPracticeController(List<String> studentList, long seed) {
        if (studentList == null) {
            throw new IllegalArgumentException("The map was null");
        }
        this.randomizer = new Random(seed);
        this.studentsNames = studentList;
        this.currentStudentName = "";
    }
    
    /**This will return the name of the next student
     * 
     * @precondition    none
     *
     * @return          The name of the next student in the list
     */
    public String getNextStudent() {
        
        this.currentStudentName = this.studentsNames.get(this.randomizer.nextInt(this.studentsNames.size()));
        return this.currentStudentName;
        
    }
}
