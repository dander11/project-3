/**
 * 
 */
package edu.westga.cs1302.projectOne.view;

import java.io.File;
import java.io.IOException;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import edu.westga.cs1302.projectOne.controller.DeleteAStudentController;
import edu.westga.cs1302.projectOne.controller.GetNextPracticeController;
import edu.westga.cs1302.projectOne.controller.GetNextStudentLearningController;
import edu.westga.cs1302.projectOne.controller.LoadStudentsDataController;
import edu.westga.cs1302.projectOne.controller.MatchPictureAndNameController;
import edu.westga.cs1302.projectOne.model.DataMap;
import edu.westga.cs1302.projectOne.model.Student;
import edu.westga.cs1302.projectOne.model.StudentDataMap;

/**This will be the go-between for the FaceTutor view and model
 * @author David Anderson
 * 
 * @version 1/27/2015
 */
public class FaceTutorViewModel { 
	

    private static final String DEFAULT_IMAGE = "unknownStudent.jpg";
	private ObservableList<String> studentList;
	private ObjectProperty<Image> studentFaceImage;

    private DataMap<String, Student> theMap;
    private LoadStudentsDataController loadController;
    private GetNextStudentLearningController nextLearningController;
    private MatchPictureAndNameController matchController;
    private String imageUrl;
    private boolean isInLearningMode;
    private GetNextPracticeController nextPracticeController;
    private DeleteAStudentController deleteController;
	
	/**
	 * Creates a new FaceTutorViewModel object with its properties initialized.
	 * 
	 * @precondition none
	 * @postcondition the object exists
	 */
	public FaceTutorViewModel() {
		Image initialImage = new Image(getClass().getResourceAsStream(DEFAULT_IMAGE));
		this.studentFaceImage = new SimpleObjectProperty<Image>(initialImage);
		this.isInLearningMode = true;
		this.theMap = new StudentDataMap();
		this.studentList = FXCollections.observableArrayList();
		
		
	}
	
	/**
	 * Returns the property that wraps the logo image.
	 * 
	 * @precondition none
	 * @return the logo image
	 */
	public Property<Image> studentImageProperty() {
		return this.studentFaceImage;
	}

	/**
	 * Returns the property that wraps the lines of text.
	 * 
	 * @precondition none
	 * @return the lines of text
	 */
	public ObservableList<String> getTextList() {
		return this.studentList;
	}
	
	/**
     * Returns the property that wraps the lines of text.
     * 
     * @precondition none
     * @return the lines of text
     */
    public ObservableList<String> getStudentNameList() {
        return this.studentList;
    }
	
	
    /**This will populate the data map from the file selected by the user
     * 
     * @param inputFile the file selected by the user
     *
     * @precondition    input file != null
     * 
     * @throws IOException  if the file cannot be read
     */
	public void loadStudentDataFrom(File inputFile) throws IOException {
        if (inputFile == null) {
            throw new IllegalArgumentException("File to load is null");
        }
        
        this.loadController = new LoadStudentsDataController(inputFile, this.theMap);
        this.loadController.loadStudentData();
       // this.matchController = new MatchPictureAndNameController(this.theMap);
        this.updateStudentList();

        this.nextPracticeController = new GetNextPracticeController(this.studentList);
        this.nextLearningController = new GetNextStudentLearningController(this.studentList);
        this.deleteController = new DeleteAStudentController(this.theMap);
    }

	private void setStudentImage(Student theStudent) {
        Image thePoster = null; 
        try {
            thePoster = new Image(theStudent.getStudentPictureUrl().toString());
        } catch (Exception invalidUrl) {
            thePoster = new Image(getClass().getResourceAsStream(DEFAULT_IMAGE));
        }
        this.studentFaceImage.setValue(thePoster);
        this.imageUrl = theStudent.getStudentPictureUrl().toString();
    }
	
    private void updateStudentList() {
        this.studentList.clear();
        this.studentList.addAll(this.theMap.keys());
        this.studentList.sort(null);
        
    }

    /**This will switch the image displayed to the next image
     *
     * @precondition    none
     * 
     * @postcondition   the displayed image will be changed
     */
    public void setNextImage() {
        if (this.isInLearningMode) {
            this.setStudentImage(this.theMap.get(this.nextLearningController.getNextStudent()));
        } else {
            this.setStudentImage(this.theMap.get(this.nextPracticeController.getNextStudent()));
        }
       
        
        
    }

    /**This will compare image currently displayed with the student name selected
     * 
     * @param selectedStudentName   The name of the student selected
     *
     * @precondition    none
     *
     * @return  true if the selected name and displayed picture belong to the same student
     */
    public boolean comparePicWithName(String selectedStudentName) {
        this.matchController = new MatchPictureAndNameController(this.theMap);
        if (this.matchController.comparePictureAndName(this.imageUrl, selectedStudentName) == 0) {
            return true;
        }
        return false;
        
    }

    /**This will switch if the program is in learning mode or not
     * 
     * @precondition    none
     * 
     * @postcondition   The program will be in a different mode
     */
    public void switchMode() {
        if (this.isInLearningMode) {
            this.isInLearningMode = false;
        } else {
            this.isInLearningMode = true;
        }
        
    }

    /**this will remove the selected student
     * 
     * @param selectedStudent   the student to be removed
     *
     * @precondition    none
     *
     * @postcondition   the student will be removed
     */
    public void remove(String selectedStudent) {
        this.deleteController.removeStudent(selectedStudent);
        this.updateStudentList();
    }

}
