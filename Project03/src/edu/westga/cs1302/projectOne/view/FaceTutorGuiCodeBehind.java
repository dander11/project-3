/**
 * 
 */
package edu.westga.cs1302.projectOne.view;

/**This will be the codebehind for the GUI
 * @author David Anderson
 * 
 * @version .05
 */
import java.io.File;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

import javax.swing.JOptionPane;

/**This will be the codebehind for the GUI
 * @author David Anderson
 * 
 * @version 1.0
 */
public class FaceTutorGuiCodeBehind {
    
    private static final String READ_DATA_ERROR_MESSAGE = 
                "ERROR: couldn't load the student data";
    private static final String ERROR_DIALOG_TITLE = 
                "Data import error";
	
	private FaceTutorViewModel theViewModel;
	
	@FXML
	private Button nextButton;

    @FXML
    private ListView<String> studentNameListView;

    @FXML
    private Text userInstructions;
    
    @FXML
    private MenuItem switchModeItem;

    @FXML
    private MenuItem deleteItem;

    @FXML
    private MenuItem openMenuItem;

    @FXML
    private ImageView studentPictureImageView;

    @FXML
    private Button selectButton;
    
    /**This will create the needed fields 
     * 
     * @param   none
     * 
     * @precondition    none
     * 
     * @postcondition   The fields will b e created
     */
    public FaceTutorGuiCodeBehind() {
    	this.theViewModel = new FaceTutorViewModel();
    }
    
    /**
	 * Initializes the GUI components, binding them to the view model properties
	 * and setting their event handlers.
	 * 
	 * @precondition   none
	 * 
	 * @postcondition  The GUI will be initialized
	 */
	@FXML
	public void initialize() {
	    this.bindComponentsToViewModel();
        this.setEventActions();
		
		
	}
	
	private void setEventActions() {
	    this.openMenuItem.setOnAction(event -> this.handleImportAction());
	    this.nextButton.setOnAction(event -> this.nextPicture());
        
	    this.selectButton.setOnAction(event -> this.handleListViewSelection());
	    this.switchModeItem.setOnAction(event -> this.switchMode());
	    this.deleteItem.setOnAction(event -> this.deleteSelectedItem());
	   
    }

    private void deleteSelectedItem() {
        String selectedStudent = this.studentNameListView.getSelectionModel().selectedItemProperty().get();
        this.theViewModel.remove(selectedStudent);
    }

    private void switchMode() {
        this.theViewModel.switchMode();
    }

    private void handleListViewSelection() {
        String selectedStudent = this.studentNameListView.getSelectionModel().selectedItemProperty().get();
        if (this.theViewModel.comparePicWithName(selectedStudent)) {
            this.nextPicture();
        } else {
            JOptionPane.showMessageDialog(null,
                   "Try Again",
                   "Wrong Answer",
                   JOptionPane.ERROR_MESSAGE);
        }
    }

    private void nextPicture() {
        this.theViewModel.setNextImage();
    }

    private void bindComponentsToViewModel() {
        this.studentNameListView.setItems(this.theViewModel.getTextList());
        
        this.studentPictureImageView.imageProperty().bindBidirectional(this.theViewModel.studentImageProperty());
    }

    private void handleImportAction() {
		FileChooser chooser = this.initializeFileChooser("Read from");

		File inputFile = chooser.showOpenDialog(null);
		if (inputFile == null) {
			return;
		}
		
		try {
            this.theViewModel.loadStudentDataFrom(inputFile);
            
        } catch (IOException readException) {
            JOptionPane.showMessageDialog(null,
                    READ_DATA_ERROR_MESSAGE,
                    ERROR_DIALOG_TITLE,
                    JOptionPane.ERROR_MESSAGE);
        }  

	}
	
	private FileChooser initializeFileChooser(String title) {
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().add(
						new ExtensionFilter("CSV Files", "*.csv"));
		chooser.setTitle(title);
		return chooser;
	}

}