/**
 * 
 */
package edu.westga.cs1302.projectOne.model;

import java.io.IOException;
import java.net.URL;

/**
 * This class will model a student in the class
 * 
 * @author David Anderson
 * 
 * @version 2/10/15
 *
 */
public class Student {
    private String name;
    private URL studentPictureUrl;

    /**This will creat the student
     *
     * @param name              The name of the student
     * @param studentPicUrl     The URL of the student's picture
     * 
     * @precondition            name && studentPicUrl != null
     *
     * @postcondition           The student will be created
     * 
     * @throws IOException      If studentPicUrl is not a valid URL
     */
    public Student(String name, String studentPicUrl) throws IOException {
        if (name == null) {
            throw new IllegalArgumentException("Invalid name.");
        }
        this.name = name;
        this.studentPictureUrl = new URL(studentPicUrl);
    }

    /**Returns the student's name
     * @precondition    none
     * @return          the name of the student
     */
    public String getName() {
        return this.name;
    }

    /**Returns the URL of the student's picture
     * @precondition    none
     * @return          the student's Picture URL
     */
    public URL getStudentPictureUrl() {
        return this.studentPictureUrl;
    }

}
