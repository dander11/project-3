/**
 * 
 */
package edu.westga.cs1302.projectOne.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs1302.projectOne.controller.LoadStudentsDataController;
import edu.westga.cs1302.projectOne.controller.GetNextStudentLearningController;
import edu.westga.cs1302.projectOne.model.DataMap;
import edu.westga.cs1302.projectOne.model.Student;
import edu.westga.cs1302.projectOne.model.StudentDataMap;

/**This will test the GetNextStudentLearningController
 * @author David Anderson
 *
 * @version Mar 25, 2015
 */
public class WhenGetNextStudentInLearningMode {
    private DataMap<String, Student> theMap;
    private File testFile;
    private GetNextStudentLearningController theNextController;
    private LoadStudentsDataController theLoadController;

    /**
     * no-op
     */
    public WhenGetNextStudentInLearningMode() {
        // no-op
    }
    
    /**This will setup the tests
     * @throws IOException If the URL for the student's picture is not valid
     * 
     * @precondition    none
     *
     */
    @Before
    public void setUp() throws IOException {
        this.testFile = new File("TestDocument.csv");
        this.theMap = new StudentDataMap();
        this.theLoadController = new LoadStudentsDataController(this.testFile, this.theMap);
        this.theLoadController.loadStudentData();
        this.theNextController = new GetNextStudentLearningController(this.theMap.keys());
    }

    /**
     * Will test the getNextStudent method
     */
    @Test
    public void firstStudentAccessedShouldBeFirstStudentInTheList() {
        assertEquals(this.theNextController.getNextStudent(), this.theMap.values().get(0).getName());
        
    }
    
    /**
     * Will test the getNextStudent method
     */
    @Test
    public void secondStudentAccessedShouldBeSecondStudentInTheList() {
        this.theNextController.getNextStudent();
        assertEquals(this.theNextController.getNextStudent(), this.theMap.values().get(1).getName());
        
    }
    
    /**
     * Will test the getNextStudent method
     */
    @Test
    public void sixthStudentAccessedShouldBeFirstStudentInTheList() {
        this.theNextController.getNextStudent();
        this.theNextController.getNextStudent();
        this.theNextController.getNextStudent();
        this.theNextController.getNextStudent();
        this.theNextController.getNextStudent();
        assertEquals(this.theNextController.getNextStudent(), this.theMap.values().get(0).getName());
        
    }

}
