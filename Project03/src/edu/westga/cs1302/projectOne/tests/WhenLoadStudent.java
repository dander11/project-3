/**
 * 
 */
package edu.westga.cs1302.projectOne.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs1302.projectOne.controller.LoadStudentsDataController;
import edu.westga.cs1302.projectOne.model.DataMap;
import edu.westga.cs1302.projectOne.model.Student;
import edu.westga.cs1302.projectOne.model.StudentDataMap;

/**This will test that the student data is loaded correctly
 * @author David Anderson
 *
 * @version Mar 25, 2015
 */
public class WhenLoadStudent {
    private DataMap<String, Student> theMap;
    private File testFile;
    private LoadStudentsDataController theController;

    /**
     * no-op
     */
    public WhenLoadStudent() {
        // no-op
    }
    
    /**This will setup the tests
     * @throws IOException If the URL for the student's picture is not valid
     * 
     * @precondition    none
     *
     */
    @Before
    public void setUp() throws IOException {
        this.testFile = new File("TestDocument.csv");
        this.theMap = new StudentDataMap();
        this.theController = new LoadStudentsDataController(this.testFile, this.theMap);
        this.theController.loadStudentData();
    }

    /**
     * this will test that five students are loaded into the map
     */
    @Test
    public void shouldLoad5Students() {
        assertEquals(5, this.theMap.size());
    }
    
    /**
     * this will test that the first student is correct
     */
    @Test
    public void shouldLoadFirstStudentInFile() {
        assertEquals(this.theMap.values().get(0), this.theMap.get("Joker"));
    }
    
    /**
     * 
     * this will test that the middle student is correct
     */
    @Test
    public void shouldLoadMiddleStudentInFile() {
        assertEquals(this.theMap.values().get(2), this.theMap.get("Bane"));
    }
    
    /**
     * this will test that the last student is correct
     */
    @Test
    public void shouldLoadLastStudentInFile() {
        assertEquals(this.theMap.values().get(4), this.theMap.get("Robin"));
    }

}
