/**
 * 
 */
package edu.westga.cs1302.projectOne.tests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs1302.projectOne.controller.GetNextPracticeController;
import edu.westga.cs1302.projectOne.controller.LoadStudentsDataController;
import edu.westga.cs1302.projectOne.model.DataMap;
import edu.westga.cs1302.projectOne.model.Student;
import edu.westga.cs1302.projectOne.model.StudentDataMap;

/**This will test when the next picture is selected in practice mode
 * @author David Anderson
 *
 * @version Mar 25, 2015
 */
public class WhenGetNextStudentInPracticeMode {
    private DataMap<String, Student> theMap;
    private File testFile;
    private GetNextPracticeController theNextController;
    private LoadStudentsDataController theLoadController;
    /**
     * no-op
     */
    public WhenGetNextStudentInPracticeMode() {
        // no-op
    }
    
    /**This will setup the tests
     * @throws IOException If the URL for the student's picture is not valid
     * 
     * @precondition    none
     *
     */
    @Before
    public void setUp() throws IOException {
        this.testFile = new File("TestDocument.csv");
        this.theMap = new StudentDataMap();
        this.theLoadController = new LoadStudentsDataController(this.testFile, this.theMap);
        this.theLoadController.loadStudentData();
        this.theNextController = new GetNextPracticeController(this.theMap.keys(), 2);
        
    }

    /**
     * This will test that the first student is at index 3
     */
    @Test
    public void firstStudentAccessedShouldBeAtIndex3() {
        assertEquals(this.theMap.get("Alfred").getName(), this.theNextController.getNextStudent());
    }
    
    /**
     * This will test that the second student is at index 2
     */
    @Test
    public void secondStudentAccessedShouldBeAtIndex2() {
        this.theNextController.getNextStudent();
        assertEquals(this.theMap.get("Bane").getName(), this.theNextController.getNextStudent());
    }
    
 

}
