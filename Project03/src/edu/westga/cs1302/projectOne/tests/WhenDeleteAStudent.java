/**
 * 
 */
package edu.westga.cs1302.projectOne.tests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs1302.projectOne.controller.DeleteAStudentController;
import edu.westga.cs1302.projectOne.controller.LoadStudentsDataController;
import edu.westga.cs1302.projectOne.model.DataMap;
import edu.westga.cs1302.projectOne.model.Student;
import edu.westga.cs1302.projectOne.model.StudentDataMap;

/**This will test when a student is deleted
 * @author David Anderson
 *
 * @version Mar 25, 2015
 */
public class WhenDeleteAStudent {
    private DataMap<String, Student> theMap;
    private File testFile;
    private DeleteAStudentController theDeleteController;
    private LoadStudentsDataController theLoadController;

    /**
     * no-op
     */
    public WhenDeleteAStudent() {
        // no-op
    }
    
    /**This will setup the tests
     * @throws IOException If the URL for the student's picture is not valid
     * 
     * @precondition    none
     *
     */
    @Before
    public void setUp() throws IOException {
        this.testFile = new File("TestDocument.csv");
        this.theMap = new StudentDataMap();
        this.theLoadController = new LoadStudentsDataController(this.testFile, this.theMap);
        this.theLoadController.loadStudentData();
        this.theDeleteController = new DeleteAStudentController(this.theMap);
    } 
    
    /**
     * test that the student was removed from the list
     */
    @Test
    public void deletedStudentShouldBeRemovedFromTheList() {
        this.theDeleteController.removeStudent("Joker");
        assertEquals(this.theMap.keys().contains("Joker"), false);
        
    }

    /**
     * test that the student was removed from the map
     */
    @Test
    public void deletedStudentShouldBeRemovedFromTheMap() {
        this.theDeleteController.removeStudent("Joker");
        assertEquals(this.theMap.contains("Joker"), false);
        
    }

}
