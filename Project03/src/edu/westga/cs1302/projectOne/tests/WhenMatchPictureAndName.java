/**
 * 
 */
package edu.westga.cs1302.projectOne.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs1302.projectOne.controller.MatchPictureAndNameController;
import edu.westga.cs1302.projectOne.model.DataMap;
import edu.westga.cs1302.projectOne.model.Student;
import edu.westga.cs1302.projectOne.model.StudentDataMap;

/**This will test that controller matches student's pictures with their name correctly
 * @author David Anderson
 *
 * @version Mar 25, 2015
 */
public class WhenMatchPictureAndName {
    private DataMap<String, Student> theMap;
    private String imgUrl;
    private String selectedStudentName;
    private MatchPictureAndNameController matchController;

    /**
     * no-op
     */
    public WhenMatchPictureAndName() {
        // no-op
    }
    
    /**This will setup the tests
     * @throws IOException If the URL for the student's picture is not valid
     * 
     * @precondition    none
     *
     */
    @Before
    public void setUp() throws IOException {
        this.theMap = new StudentDataMap();
        this.theMap.add("Joker", new Student("Joker", "http://fc00.deviantart.net/fs71/f/2012/208/f/a/joker_painting_by_scampicrevette-d58soyl.jpg"));
        this.theMap.add("Batman", new Student("Batman", "http://imageserver.moviepilot.com/batman-batman-vs-superman-vote-for-your-favorite-batman-costume-a2525f6d-50ea-4216-8e55-6d7fbd9f5d7e.jpeg?width=660&height=391"));
        this.imgUrl = "http://imageserver.moviepilot.com/batman-batman-vs-superman-vote-for-your-favorite-batman-costume-a2525f6d-50ea-4216-8e55-6d7fbd9f5d7e.jpeg?width=660&height=391";
        this.matchController = new MatchPictureAndNameController(this.theMap);
    }

    /**
     * Will test when the picture and name should match
     */
    @Test
    public void shouldMatchIfNamesTheSame() {
        this.selectedStudentName = "Batman";
        assertEquals(0, this.matchController.comparePictureAndName(this.imgUrl, this.selectedStudentName));
    }
    
    /**
     * Will test when the picture and name should match
     */
    @Test
    public void shouldNotMatchIfNamesAreDifferent() {
        this.selectedStudentName = "Joker";
        assertNotEquals(1, this.matchController.comparePictureAndName(this.imgUrl, this.selectedStudentName));
    }

}
