/**
 * 
 */
package edu.westga.cs1302.projectOne.driver;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**This will run the program
 * @author David Anderson
 * 
 * @version 1/12/2015
 */
public class FaceTutorDriver extends Application {
	
	private static final String WINDOW_TITLE = "Face Tutor";
	private static final String GUI_FXML = "../view/FaceTutorGui.fxml";

	/**
	 * Constructs a new Application object for the login demo
	 * program.
	 * 
	 * @precondition	none
	 * @postcondition	the object is ready to execute
	 */
	public FaceTutorDriver() {
		super();
	}

	/**The main entry point for all JavaFX applications. 
	 * The start method is called after the init method has 
	 * returned, and after the system is ready for the application to begin running. 
	 * 
	 * @param primaryStage the primary stage for this application
	 *
	 * @precondition   None
	 *
	 * @postcondition  The application will start
	 */
	public void start(Stage primaryStage) {
		try {
			Pane pane = this.loadGui();
			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.show();
		} catch (IllegalStateException | IOException anException) {
			anException.printStackTrace();
		}
	}

	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}
	/**
	 * Launches the application.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
